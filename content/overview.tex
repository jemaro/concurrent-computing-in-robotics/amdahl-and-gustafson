\section{Overview}
\label{sec:overview}
In this section we will overview Gustafson's law, which is a reevaluation of
Amdahl's \nameref{subsec:fixed-size} model into a \nameref{subsec:fixed-time}
model. We will close the section by a demonstration of their
\nameref{subsec:equivalence}.

\subsection{Fixed size}
\label{subsec:fixed-size}
\citeauthor{amdahl1967} theorized that the possible \emph{speedup} of
parallelizing a program was restricted by its serial processing percentage
relative to the overall execution time using a single processor. Therefore the
program execution time in a single processor can be defined as the sum of its
serial and \emph{parallelizable} parts as illustrated in
\cref{fig:fixed-size-model.drawio}, where $Time(n)$ is the time required to
execute a program with $n$ processors. 

Even though \citeauthor{amdahl1967} did not derive any analytical formulation
in his original work, we believe that it helps to understand the concepts
better. The serial percentage of the program execution in a single processor is
defined as $s_{FS}$. Being the rest of the program \emph{parallelizable}
$p_{FS} = 1 - s_{FS}$. If the program is executed in parallel across $N$
processors, the total execution time of the same program can be deduced into
\cref{eq:FS-TN}. Where the time to execute the serial part remains constant,
while the \emph{parallelizable} part is distributed across $N$ processors.

\begin{equation}
    Time(N) = Time(1) * (s_{FS} + \frac{p_{FS}}{N})
    \label{eq:FS-TN}
\end{equation}

As we have assumed the same program, the work performed is fixed. We can
finally deduce the speedup of the parallel execution using its definition from
\cref{eq:speedupN} together with \cref{eq:FS-TN} arriving to
\cref{eq:FS-speedup}. \Cref{fig:speedup-fixed-size} illustrates the fixed size
speedup of a multicore architecture with increasing number of cores.

\begin{equation}
    Speedup(N) = 1 / (s_{FS} + \frac{p_{FS}}{N})
    \label{eq:FS-speedup}
\end{equation}

\simplefigure{fixed-size-model.drawio.png}{
    Fixed size model where the serial fraction of the execution time is defined
    in the single-processor execution $Speedup(N) = 1/(s_{FS} +
    \protect\frac{p_{FS}}{N})$.
}

\simplefigure{speedup-fixed-size.png}{
    Fixed-size speedup of multicolre architectures \protect\cite{sun2008}. The
    horizontal axis represents the number of cores, scaled from 1 to 256. The
    vertical axis represents the speedup value as defined in
    \protect\cref{eq:FS-speedup} where $f$ is the fraction of the
    \emph{parallelizable} single-core execution time $f = p_{FS} = 1 - s_{FS}$.
}

\subsection{Fixed time}
\label{subsec:fixed-time}
If the time is fixed to be the same in a single-processor and parallel
execution, then the parallel execution would perform more work than the
single-processor execution. We can then compare the speedup of the parallel
execution with respect to an hypothetical single-processor execution that
performs the same work. \Cref{fig:fixed-time-model.drawio} illustrates this
concept, highlighting that now the parallel and serial fractions ($s_{FT}$ and
$p_{FT}$, respectively) are measured in the parallel execution time.

We can then define the work performed by the parallel execution with respect to
a single-processor execution with the same time as \cref{eq:FT-WN}. Using it
with \cref{eq:speedupN} we can deduce \cref{eq:FT-speedup}. Which is the
parallel execution speedup of a fixed time model. Also known as Gustafson's Law
\cite{gustafson1988}. \Cref{fig:speedup-fixed-time} illustrates the fixed time
speedup of a multicore architecture with increasing number of cores.

\begin{equation}
    Work(N) = Work(1) * (s_{FT} + N p_{FT})
    \label{eq:FT-WN}
\end{equation}

\begin{equation}
    Speedup(N) = s_{FT} + N p_{FT}
    \label{eq:FT-speedup}
\end{equation}


\begin{figure*}
    \centering
    \includegraphics[width=\textwidth]{\subdir/fixed-time-model.drawio.png}
    \caption{Fixed time model where the serial fraction of the execution time
    is defined in the parallel processor execution $Speedup(N) = s_{FT} + N
    p_{FT}$.}
    \label{fig:fixed-time-model.drawio}
\end{figure*}

\simplefigure{speedup-fixed-time.png}{
    Fixed-time speedup of multicolre architectures \protect\cite{sun2008}. The
    horizontal axis represents the number of cores, scaled from 1 to 1024. The
    vertical axis represents the speedup value as defined in
    \protect\cref{eq:FT-speedup} where $f$ is the fraction of the parallel
    execution time $f = p_{FT} = 1 - s_{FT}$.
}


\subsection{Equivalence}
\label{subsec:equivalence}
Amdahl's and Gustafson's laws are actually equivalent. The key difference is
that both laws define in a different way the serial fraction of the program
execution. While the fixed size model defines the serial fraction $s_{FS}$ as
the percentage of the program execution time \emph{in a single processor},
Gustafson's fixed time defined it as the percentage of the program execution
time \emph{in the parallel execution}.

What it is clear is that the execution time of a program parallelized over $n$
units is the sum of its serial and parallelized parts. The execution time
depends on the number of units $n$, but its serial part is independent of it,
only the parallelizable part of the program will change with the number of
units. Which is illustrated in \cref{eq:time-definition}.

\begin{equation}
    Time(n) = Time_{Serial} + Time_{Parallel}(n)
    \label{eq:time-definition}
\end{equation}

As the serial execution time is constant, both definitions of the serial
fraction of the execution time ($s_{FS}$ for the fixed size model and $s_{FT}$
for the fixed time model) can be easily related with
\cref{eq:constant-serial-time}. 

\begin{equation}
    Time_{Serial} = Time(1) * s_{FS} = Time(N) * s_{FT}
    \label{eq:constant-serial-time}
\end{equation}

The parallelizable part of the execution time is ideally linearly distributed
across the number of units $N$. Therefore a fully parallelized part of the
program over $N$ units would last $N$ times less than if it was not
parallelized, as it is shown in \cref{eq:parallel-time-equivalence}. If we
substitute with the corresponding definitions of the parallel fraction of the
execution time ($p_{FS}$ for the fixed size model and $p_{FT}$ for the fixed
time model) we can obtain \cref{eq:parallel-percentage-equivalence}.

\begin{equation}
    Time_{Parallel}(N) = Time_{Parallel}(1) / N
    \label{eq:parallel-time-equivalence}
\end{equation}

\begin{equation}
    Time(N) * p_{FT} = Time(1) * p_{FS} / N
    \label{eq:parallel-percentage-equivalence}
\end{equation}

Combining \cref{eq:constant-serial-time,eq:parallel-percentage-equivalence} we
can deduce \cref{eq:parallel-FS-FT-equivalence}, an equivalence of the parallel
fraction of the execution time as defined in the fixed size model $p_{FS}$ with
the analogous definition from the fixed time model $p_{FT}$. Don't forget that
the parallel and serial fractions are complementary in their corresponding
definitions ($s_{FS} + p_{FS} = 1$ and $s_{FT} + p_{FT} = 1$), therefore we can
easily deduce \cref{eq:serial-FS-FT-equivalence}, an equivalence of the serial
fraction of the execution time as defined in the fixed size model $s_{FS}$ with
the fixed time model.

\begin{equation}
    p_{FS} = \frac{N p_{FT}}{s_{FT} + N p_{FT}}
    \label{eq:parallel-FS-FT-equivalence}
\end{equation}

\begin{equation}
    s_{FS} = \frac{s_{FT}}{s_{FT} + N p_{FT}}
    \label{eq:serial-FS-FT-equivalence}
\end{equation}

We can use \cref{eq:parallel-FS-FT-equivalence,eq:serial-FS-FT-equivalence} to
deduce Gustafson's law (\cref{eq:FT-speedup}) starting from Amdahl's law
(\cref{eq:FS-speedup}), which demonstrates that the fixed size and fixed time
model are equivalent. This procedure is illustrated in \cref{eq:equivalence}.

\begin{equation}
    \begin{split}
    Speedup(N) &= \left(s_{FS} + \frac{p_{FS}}{N}\right)^{-1} \\
    &= \left(\frac{s_{FT}}{s_{FT} + N p_{FT}} 
    + \frac{p_{FT}}{s_{FT} + N p_{FT}}\right)^{-1} \\
    &= s_{FT} + N p_{FT}
    \end{split}
    \label{eq:equivalence}
\end{equation}

Let's put some numbers to see this equivalence in practice. If we have a fixed
time model parallel execution with $N = 256$ cores and a parallel fraction of
$p_{FT} = 0.8$, the corresponding speedup can be computed with
\cref{eq:FT-speedup}. Which reaches the value of $205$, corresponding to the
what we can observe in \cref{fig:speedup-fixed-time}. But it should not be
compared to a fixed size model with the same parallel fraction of execution
time $p_{FS} \neq p_{FT} = 0.8$. Instead the fixed size equivalence can be
computed with \cref{eq:parallel-FS-FT-equivalence}, $p_{FS} = 0.999024$. Now
the corresponding speedup can be calculated with \cref{eq:FS-speedup}, reaching
the same value of $205$, illustrated as well in \cref{fig:speedup-fixed-size}.

Amdahl pointed out that the speedup of parallelizing a program is restricted by
its serial processing percentage and Gustafson did not prove him wrong, but he
argued that if we scale up the size of the problem, this serial processing
percentage can be reduced.

% \simplefigure{speedup-comparison.png}{\hl{TODO}\protect\cite{sun2008}}