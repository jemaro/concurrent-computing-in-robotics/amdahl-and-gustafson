\section{Relevance Today}
\label{sec:relevance-today}
In this section, we will discuss the relevance of Gustafson's law in the actual
context by first highlighting the increased importance of parallel computing
due to \nameref{subsec:the-end-of-dennards-scaling}. Then we will highlight the
existence of a \nameref{subsec:superlinear-speedup} and some of its causes.
Finally, we will put these computing laws in the context of
\nameref{subsec:multi-robot-systems}.

\subsection{The end of Dennard's Scaling}
\label{subsec:the-end-of-dennards-scaling}
\emph{Reducing the critical dimensions while keeping the electrical field
constant yields higher speed and a reduced power consumption of a digital MOS
circuit} \cite{dennard1974}. Dennard proposed that reducing the transistor
dimensions would compensate the trend to increase the number of transistors and
its operating frequency in terms of power consumption. Amdahl's
\cite{amdahl1967} and Gustafson's \cite{gustafson1988} laws where formulated in
the 1970s and 1990s respectively. While Gustafson's law was optimistic about
massive parallelism, the reality was that single-threaded performance followed
Moore's law \cite{moore1665} (doubling every two years). Therefore there was no
general need for parallel computing.

\simplefigure{50-years-processor-trend.png}{Microprocessor trend data of the
last 50 years.}

In 2005 we reached an inflection point where reducing the critical dimensions
was not feasible anymore, mainly due to subthreshold leakage current. As it can
be seen in \cref{fig:50-years-processor-trend}, Moore's law still holds, the number
of transistors is doubling every two years so far. But power consumption scales
up together with the number of transistors. This brings a new limitation of the
number of transistors in a single core that depends on the maximum temperatures
that they can withstand and its cooling system.

Increasing the number of logical cores can bypass that limitation, which makes
parallel computing the economically efficient option for once. Due to that, it
is brought into general purpose computing.

\subsection{Superlinear speedup}
\label{subsec:superlinear-speedup}
We have seen that the speedup obtained from parallelization is limited by
the non parallelizable fraction of a problem. In theory, the ideal
case would be a linear speedup with the number of units $N$. While
single-processor computing was not limited by this. Increasing the clock
frequency would mean a linear speedup.

In the practice, certain systems can benefit from interactions between units to
cause the \emph{Speedup} of a system of $N$ units as defined in
\cref{eq:speedupN} to be greater than $N$ \cite{gunther1993}. There are many
spurious sources of superlinear speedup, for example \emph{loop overhead}. But
these are ephemeral and not particularly interesting. In this work we collect
two important sources of superlinear speedup.

\paragraph{Combined units} 
Parallel computing laws assume that the units are \emph{pure} and only add
resources to the system in one dimension (e.g., an additional CPU only adds a
computation module). While in reality units are usually \emph{combined} (a CPU
includes registers and some cache memory) \cite{hamann2021}. Continuing the CPU
example, superlinear speedup can result whenever the problem size per CPU is
reduced such that it fits into its the cache memory \cite{gustafson1990fixed}.
Most people react to this form of superlinear speedup as anomalous, like an
improperly run experiment. But it is an important effect to be taken into
account in practical applications.

% \paragraph{Changing Routine Profile}
% When a problem is composed of different routines of different complexity,
% scaling them up may cause \emph{more time to be spent in faster routines}
% \cite{gustafson1990fixed}. As shown in \cref{fig:changing-routine-profile},
% increasing the problem size might increase the fraction of time spent in faster
% parts of the routine, increasing the overall routine speed.

% \simplefigure{changing-routine-profile.png}{ 
%     Routine fraction (vertical axis) versus problem size $N$ (horizontal axis)
%     for an example scientific problem (Computing the radiosity on the interior
%     of a box for realistic scene rendering) composed of input of the geometry
%     from disk and output of the solution on disk $I/O$ with complexity $O(n)$; 
%     \emph{setup} of the matrix representing the equations with complexity 
%     $O(n^2)$; and \emph{solving} the equations with complexity $O(n^3)$.
% }

\paragraph{Non-structure persistent algorithms}
An assumption of Amdahl's and Gustafson's formulation is that the serial
and parallel \emph{programs} take the same number of total calculation steps
for the same input. In reality, however, some parallel implementations can
bypass large amount of calculation steps while yielding the same output of the
corresponding serial algorithm \cite{shi1996}.

\subsection{Multi Robot Systems} 
\label{subsec:multi-robot-systems}
So far we have talked about parallelizing computing tasks, however, these
concepts are applicable to other types of problems and systems. In this section
we will apply them to robotic systems, which have moved from a developing
technology to an indispensable tool for a large number of fields.

Multi-robot systems have emerged as a suitable alternative to single robots to
improve current and enable new missions \cite{roldan2021}. These systems are
more flexible and fault tolerant than single robot systems. But they also can
benefit from parallelism in a very similar way computing does.

As in computing systems, speeding up a single robot system might not be
economically viable compared to a multi-robot system anymore. However,
parallelizing robotic systems have increased limitations compared to computing,
for example, communication lag is orders of magnitude higher between units when
they are not physically connected.

At the same time, multi-robot systems might be more likely to achieve
superlinear speedups. Robots are highly \emph{combined} units (sensors,
actuators, energy access and storage, computational resources, etc.), which, as
we have already mentioned, is an important source of superlinear speedup.
Moreover, there are many applications where multiple robot algorithms can solve
a task in few steps than its single robot counterparts. Imagine a robotic
system to push boxes of several sizes. A simple car-like robot can push a box
in a straight line and reorient himself around the box in order to follow a
given path. However a parallelized implementation of that algorithm can avoid
the reorienting steps by using a couple of car-like robots pushing from
different edges of the box. Which can result in a superlinear speedup.
