\section{Introduction}
\label{sec:introduction}

Many engineered systems can improve their performance by parallelizing the
execution of tasks among its constituent units. Increasing the system size $N$
(number of units) can lead to an improvement in the system performance.

The improvement in the system performance is usually called \emph{Speedup}. It
is usually as \emph{the ratio of the individual execution time to the execution
time on parallel units}. But \emph{Speedup} can be better defined as a ratio of
\emph{speeds} as defined by \cref{eq:speedup} \cite{gustafson1990fixed}, not a
ratio of times. \emph{Speed} is work divided by time, where work can be defined
as instructions, floating point operations or whatever seems a reasonable
currency on a given system. For example, in a 3D printing task, work can be
defined as the amount of material that is extruded. An analytical definition of
\emph{speedup} depending on the number of units $N$ is presented in
\cref{eq:speedupN}.

\begin{equation}
    Speedup = \left(\frac{\text{Parallel work}}{\text{Parallel time}}\right)/
    \left(\frac{\text{Individual work}}{\text{Individual time}}\right)
    \label{eq:speedup}
\end{equation}

\begin{equation}
    Speedup(N) = \left(\frac{\text{Work(N)}}{\text{Time(N)}}\right)/
    \left(\frac{\text{Work(1)}}{\text{Time(1)}}\right)
    \label{eq:speedupN}
\end{equation}

Said \emph{work} is traditionally referred as the \emph{problem size}. That is
why \emph{speedup} is usually defined as a ratio of times, arguing that the
problem size remains constant. We will discuss that in \cref{sec:overview},
where we will differentiate between \nameref{subsec:fixed-size} and
\nameref{subsec:fixed-time} models.

\emph{Efficiency} in parallel computing is traditionally defined as
\emph{speedup divided by the number of processors $N$}. The definition assumes
the impossibility of superlinear speedup, guaranteeing that efficiency cannot
exceed unity. \Cref{sec:relevance-today} brings up the existence of
\nameref{subsec:superlinear-speedup}. This advocates that traditionally defined
\emph{efficiency} in parallel computing is a misnomer, as speedup divided by
the number of processors exceeds unity in practice. That section also
highlights the importance of parallel computing with
\nameref{subsec:the-end-of-dennards-scaling} and brings computing laws to the
context of \nameref{subsec:multi-robot-systems}.